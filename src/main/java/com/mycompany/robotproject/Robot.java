/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

/**
 *
 * @author sairu
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
    
    public Robot(int x,int y,int bx,int by,int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    public boolean walk(char direction){
        switch(direction){
            case 'N':
                y = y - 1;
            break;
            case 'S':
                y = y + 1;
            break;
            case 'E':
                x = x + 1;
            break;
            case 'W':
                x = x - 1;
            break;
        }
        lastDirection = direction;
        return true;
    }
    public boolean walk(char direction,int step){
        for(int i = 0;i<step;i++){
            if(!this.walk(direction)){
                return false;
            }
        }
        return true;
    }
    public boolean walk(){
        return this.walk(lastDirection);
    }
    public boolean walk(int step){
        return this.walk(lastDirection, step);
    }
    public String toString(){
        return "Robot ("+this.x+","+this.y+")";
    }
}
